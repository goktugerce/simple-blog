/**
 * Created by goktug on 28/12/15.
 */
"use strict";
const Mongoose = require("mongoose");
var Slug = require("slug");

const PostSchema = new Mongoose.Schema({
    owner: {
        type: String,
        required: true,
        ref: "User"
    },
    category: {
        type: String,
        required: true,
        ref: "Category"
    },
    title: {
        type: String,
        required: true
    },
    title_slug: {
        type: String,
        unique: true,
        trim: true,
        lowercase: true
    },
    content: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});


PostSchema.pre("save", function (next) {
    let post = this;
    if (post.isModified("title") || !post.title_slug) {
        return post.createSlug(next);
    }
    return next();
});

PostSchema.methods.createSlug = function (next) {
    let post = this;
    let urlSlug = Slug(post.title, {lower: true});

    mechanic.models["Post"].findOne({
        title_slug: urlSlug
    }, function (errors, collapsePost) {
        if (!errors && (!collapsePost || collapsePost._id == post._id)) {
            post.title_slug = urlSlug;
            return next();
        }

        post.title_slug = urlSlug + "_" + post._id;
        return next();
    });
}

module.exports = Mongoose.model("Post", PostSchema);