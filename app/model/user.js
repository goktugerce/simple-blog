const Mongoose = require("mongoose");
const Bcrypt = require("bcrypt");

const UserSchema = new Mongoose.Schema({
    username: {
        type: String,
        required: true,
        index: {
            unique: true
        }
    },
    email: {
        type: String,
        required: true,
        index: {
            unique: true
        }
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        default: "user",
    },
    picture: {
        type: String,
        default: "http://placehold.it/380x500"
    }
});

UserSchema.pre("save", function (next) {
    const user = this;
    if (!user.isModified("password")) {
        return next();
    }

    Bcrypt.genSalt(10, function (err, salt) {
        if (err) {
            return next(err);
        }

        Bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) {
                return next(err);
            }

            user.password = hash;
            next();
        });
    });
});

UserSchema.methods.validatePassword = function (password, next) {
    var manager = this;
    Bcrypt.compare(password, manager.password, function (err, result) {
        if (err || !result) {
            return next(err);
        }
        next(null, result);
    });
};

module.exports = Mongoose.model("User", UserSchema);