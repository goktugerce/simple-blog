/**
 * Created by goktug on 28/12/15.
 */
"use strict";
const Mongoose = require("mongoose");
const Slug = require("slug")

const CategorySchema = new Mongoose.Schema({
    name: {
        type: String,
        required: true,
        index: {
            unique: true
        }
    },
    name_slug: {
        type: String,
        unique: true,
        trim: true,
        lowercase: true
    },
});

CategorySchema.pre("save", function (next) {
    let category = this;
    if (category.isModified("name") || !category.name_slug) {
        return category.createSlug(next);
    }
    return next;
});

CategorySchema.methods.createSlug = function (next) {
    let category = this;
    let urlSlug = Slug(category.name, {lower: true});

    mechanic.models["Category"].findOne({
        name_slug: urlSlug
    }, function (errors, collapseCategory) {
        if (!errors && (!collapseCategory || collapseCategory._id == category._id)) {
            category.name_slug = urlSlug;
            return next();
        }

        category.name_slug = urlSlug + "_" + category._id;
        return next();
    });
}

module.exports = Mongoose.model("Category", CategorySchema);