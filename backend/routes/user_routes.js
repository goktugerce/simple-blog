/**
 * Created by goktug on 25/12/15.
 */
"use strict";

const User = mechanic.models["User"];

const Register = require("./user/signup");
const Login = require("./user/signin");
const Logout = require("./user/signout");

let user_routes = [];
user_routes = user_routes.concat(Register);
user_routes = user_routes.concat(Login);
user_routes = user_routes.concat(Logout);
module.exports = user_routes;