"use strict";
const Async = require("async");
const _ = require("lodash");

const Post = mechanic.models["Post"];
const User = mechanic.models["User"];
const Category = mechanic.models["Category"];

const Categories = require("./admin/categories");
const Posts = require("./admin/posts");
const Users = require("./admin/users");

const dateGetter = require("../utils/filter_dates");
const countPosts = require("../utils/count_posts")
const dateFormatter = require("../utils/format_dates");

let routes = [
    {
        method: "GET",
        path: "/user/{username}",
        handler: (request, reply) => {
            const user = request.session.get("user");
            const name = request.params.username;
            Async.auto({
                get_Categories: function (callback) {
                    Category.find({}, callback);
                },
                get_Posts: function (callback) {
                    Post.find({}, callback).populate("owner category");
                },
                get_User: function (callback) {
                    User.findOne({username: name}, callback)
                }
            }, function (err, results) {
                if (err) {
                    console.error(err);
                }


                var posts = _.filter(results.get_Posts, function (post) {
                    return post.owner.username == name;
                });

                var categories = countPosts(results.get_Categories, results.get_Posts);

                return reply.view("user", {
                    user,
                    posts,
                    postCount: posts.length,
                    profile: results.get_User,
                    categories: categories,
                    dates: dateGetter.getDates(results.get_Posts),
                });
            });
        }
    }
];

module.exports = routes;