/**
 * Created by goktug on 28/12/15.
 */
"use strict";
const Post = mechanic.models["Post"];
const Category = mechanic.models["Category"];
const User = mechanic.models["User"];
const Async = require("async");
const _ = require("lodash");
const Express = require("express");
const Multer = require("multer");

const AddPost = require("./controllers/newpost");
const ImageUpload = require("../../../utils/image_uploader");

module.exports = [
    {
        method: "GET",
        path: "/admin/posts/",
        handler: (request, reply) => {
            const user = request.session.get("user");
            if (!user || user.role === "user") {
                return reply.redirect("/admin/signin/");
            }

            Async.auto({
                get_Posts: function (callback) {
                    Post.find({}).populate("owner category").exec(callback);
                },
                get_Users: function (callback) {
                    User.find({}, callback);
                }
            }, function (err, results) {
                if (err) {
                    console.error(err);
                }

                reply.view("admin/posts/list", {
                    user: user,
                    panel: "admin",
                    posts: results.get_Posts
                })
            });
        }
    },
    {
        method: "GET",
        path: "/admin/posts/new/",
        handler: (request, reply) => {
            const user = request.session.get("user");
            if (!user || user.role === "user") {
                return reply.redirect("/admin/signin/");
            }

            Category.find({}, function (err, categories) {
                if (!err) {
                    reply.view("admin/posts/new_post", {
                        user: user,
                        panel: "admin",
                        categories: categories
                    });
                }
                else {
                    console.log("An error occurred");
                }
            });

        }
    },
    {
        method: "POST",
        path: "/admin/posts/new/upload_image",
        config: {
            payload: {
                output: "file",
                parse: true,
                allow: "multipart/form-data"
            },
            handler: (request, reply) => {
                var data = request.payload["file_path"];

                ImageUpload(data.path, function (error, result) {
                    if (error) {
                        console.log("err" + error);
                    }
                    console.log("in handler");
                    return reply(result);
                });
            }
        }
    },
    {
        method: "POST",
        path: "/admin/posts/new/create",
        handler: AddPost.create
    }
];