/**
 * Created by goktug on 25/12/15.
 */

const User = mechanic.models["User"];

module.exports = [
    {
        method: "GET",
        path: "/admin/signin/",
        handler: (request, reply) => {
            const user = request.session.get("user");

            if (user && user.role === "admin") {
                return reply.redirect("/admin/");
            }

            return reply.view("admin/sign_in");
        }
    }
]