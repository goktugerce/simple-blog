/**
 * Created by goktug on 28/12/15.
 */
"use strict";
const Post = mechanic.models["Post"];
const Category = mechanic.models["Category"];
const User = mechanic.models["User"];
const Async = require("async");
const _ = require("lodash");

module.exports = [
    {
        method: "GET",
        path: "/admin/users/",
        handler: (request, reply) => {
            const user = request.session.get("user");
            if (!user || user.role === "user") {
                return reply.redirect("/admin/signin/");
            }

            Async.auto({
                get_users: function (callback) {
                    User.find({}, callback);
                },
                get_posts: function (callback) {
                    Post.find({}, callback);
                }
            }, function (err, results) {
                if (err) {
                    console.error(err);
                }

                let usersLength = results.get_users.length;
                for (let i = 0; i < usersLength; i++) {
                    results.get_users[i].post_count = _.filter(results.get_posts, function (post) {
                        return post.owner == results.get_users[i]._id;
                    }).length;
                }

                reply.view("admin/users", {
                    user: user,
                    panel: "admin",
                    users: results.get_users
                })
            });
        }
    }
];