const Category = mechanic.models["Category"];

module.exports = {
    create: (request, reply) => {

        const categoryname = request.payload["categoryname"];

        if (!categoryname) {
            request.session.flash("error", "Category name field is empty.");
            return reply.redirect(request.info.referrer);
        }

        Category.create({
            name: categoryname
        }, (error, user) => {

            if (error) {
                console.log(error);
                request.session.flash("error", "Category already exists");
                return reply.redirect(request.info.referrer);
            }

            request.session.flash("success", "Successfully added category");
            return reply.redirect("/admin/categories/");

        });
    }
}