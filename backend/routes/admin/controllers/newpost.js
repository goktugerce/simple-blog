/**
 * Created by goktug on 28/12/15.
 */
const Category = mechanic.models["Category"];
const Post = mechanic.models["Post"];
const User = mechanic.models["User"];
const Mongoose = require("mongoose");

module.exports = {
    create: (request, reply) => {
        const title = request.payload["title"];
        const content = request.payload["content"];
        const category_id = request.payload["category"];
        const owner_id = request.payload["owner"];

        if (!title) {
            request.session.flash("error", "Title field is empty.");
            return reply.redirect(request.info.referrer);
        }

        if (!content) {
            request.session.flash("error", "Content is empty.");
            return reply.redirect(request.info.referrer);
        }

        if (!category_id) {
            request.session.flash("error", "Category is not selected.");
            return reply.redirect(request.info.referrer);
        }

        if (!owner_id) {
            request.session.flash("error", "User is not selected.");
            return reply.redirect(request.info.referrer);
        }

        Post.create({
            title: title,
            content: content,
            owner: owner_id,
            category: category_id
        }, (error, user) => {
            if (error) {
                console.log(error);
                request.session.flash("error", "Already exists");
                return reply.redirect(request.info.referrer);
            }

            request.session.flash("success", "Successfully added post");

            return reply.redirect("/admin/posts/");
        });
    }
}