/**
 * Created by goktug on 28/12/15.
 */
"use strict";
const Post = mechanic.models["Post"];
const Category = mechanic.models["Category"];
const Async = require("async");
const _ = require("lodash");

const AddCategory = require("./controllers/newcategory");

module.exports = [
    {
        method: "GET",
        path: "/admin/categories/",
        handler: (request, reply) => {
            const user = request.session.get("user");
            if (!user || user.role === "user") {
                return reply.redirect("/admin/signin/");
            }

            Async.auto({
                get_categories: function (callback) {
                    Category.find({}, callback);
                },
                get_posts: function (callback) {
                    Post.find({}, callback);
                }
            }, function (err, results) {
                if (err) {
                    console.error(err);
                }
                var categoriesLength = results.get_categories.length;
                for (var i = 0; i < categoriesLength; i++) {
                    results.get_categories[i].post_count = _.filter(results.get_posts, function (post) {
                        return post.category == results.get_categories[i]._id;
                    }).length;
                }

                reply.view("admin/categories/list", {
                    user: user,
                    panel: "admin",
                    categories: results.get_categories
                })
            });
        }
    },
    {
        method: "GET",
        path: "/admin/categories/new/",
        handler: (request, reply) => {
            const user = request.session.get("user");
            if (!user || user.role === "user") {
                return reply.redirect("/admin/signin/");
            }

            return reply.view("admin/categories/new_category", {
                user: user,
                panel: "admin"
            });
        }
    },
    {
        method: "POST",
        path: "/admin/categories/new/create",
        handler: AddCategory.create
    }
];