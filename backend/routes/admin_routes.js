/**
 * Created by goktug on 25/12/15.
 */
"use strict";

const User = mechanic.models["User"];
const Category = mechanic.models["Category"];

const Login = require("./admin/signin");
const Categories = require("./admin/categories");
const Posts = require("./admin/posts");
const Users = require("./admin/users");

let routes = [
    {
        method: "GET",
        path: "/admin/",
        handler: (request, reply) => {
            const user = request.session.get("user");
            if (!user || user.role === "user") {
                return reply.redirect("/admin/signin/");
            }

            return reply.view("admin/index", {
                user: user,
                panel: "admin"
            });
        }
    }
];

module.exports = routes.concat(Login).concat(Categories).concat(Posts).concat(Users);