"use strict";
const Async = require("async");
const _ = require("lodash");

const Post = mechanic.models["Post"];
const User = mechanic.models["User"];
const Category = mechanic.models["Category"];

const Categories = require("./admin/categories");
const Posts = require("./admin/posts");
const Users = require("./admin/users");

const dateGetter = require("../utils/filter_dates");
const countPosts = require("../utils/count_posts")
const dateFormatter = require("../utils/format_dates");

let routes = [
    {
        method: "GET",
        path: "/archive/{month}/{year}/",
        handler: (request, reply) => {
            const user = request.session.get("user");
            const month = request.params.month;
            const year = request.params.year;
            var dateFormat = month + "-" + year;
            Async.auto({
                get_Categories: function (callback) {
                    Category.find({}, callback);
                },
                get_Posts: function (callback) {
                    Post.find({}, callback).populate("owner category");
                }
            }, function (err, results) {
                if (err) {
                    console.error(err);
                }

                var posts = _.filter(results.get_Posts, function (post) {
                    var postDate = post.date;
                    postDate = dateFormatter(postDate, "MMMM-YY");
                    postDate = postDate.toLowerCase();
                    return dateFormat == postDate;
                }).reverse();

                var categories = countPosts(results.get_Categories, results.get_Posts);

                return reply.view("archive", {
                    user,
                    posts,
                    categories: categories,
                    dates: dateGetter.getDates(results.get_Posts),
                });
            });
        }
    }
];

module.exports = routes;