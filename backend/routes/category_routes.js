"use strict";
const Async = require("async");
const _ = require("lodash");

const Post = mechanic.models["Post"];
const User = mechanic.models["User"];
const Category = mechanic.models["Category"];

const Categories = require("./admin/categories");
const Posts = require("./admin/posts");
const Users = require("./admin/users");

const dateGetter = require("../utils/filter_dates");
const countPosts = require("../utils/count_posts")

let routes = [
    {
        method: "GET",
        path: "/category/{slug_name}/",
        handler: (request, reply) => {
            const page = 0;
            categoryListing(request, reply, page);
        }
    },
    {
        method: "GET",
        path: "/category/{slug_name}/{pageNr}/",
        handler: (request, reply) => {
            const page = request.params.pageNr - 1;
            categoryListing(request, reply, page);
        }
    }
];

function categoryListing(request, reply, page) {
    const user = request.session.get("user");
    const slug_name = request.params.slug_name;
    Async.auto({
        get_Categories: function (callback) {
            Category.find({}, callback);
        },
        get_Posts: function (callback) {
            Post.find({}, callback).populate("owner category");
        }
    }, function (err, results) {
        if (err) {
            console.error(err);
        }

        var posts = _.filter(results.get_Posts, function (post) {
            return post.category.name_slug == slug_name
        }).reverse();

        var count = posts.length;
        posts = posts.slice(page*3, (page+1)*3);

        var categories = countPosts(results.get_Categories, results.get_Posts);

        return reply.view("category", {
            page,
            user,
            posts,
            category: slug_name,
            categories: categories,
            dates: dateGetter.getDates(results.get_Posts),
            totalPosts: count
        });
    });
}

module.exports = routes;