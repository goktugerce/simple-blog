"use strict";
const Async = require("async");
const _ = require("lodash");

const Post = mechanic.models["Post"];
const User = mechanic.models["User"];
const Category = mechanic.models["Category"];

const Categories = require("./admin/categories");
const Posts = require("./admin/posts");
const Users = require("./admin/users");

const dateGetter = require("../utils/filter_dates");
const countPosts = require("../utils/count_posts")

let routes = [
    {
        method: "GET",
        path: "/{slug_name}/",
        handler: (request, reply) => {
            const user = request.session.get("user");
            const slug_name = request.params.slug_name;
            Async.auto({
                get_Post: function (callback) {
                    Post.findOne({"title_slug": slug_name}, callback).populate("owner category");
                },
                get_Categories: function (callback) {
                    Category.find({}, callback);
                },
                get_Posts: function (callback) {
                    Post.find({}, callback).populate("owner category");
                }
            }, function (err, results) {
                if (err) {
                    console.error(err);
                }

                var post = results.get_Post;

                var categories = countPosts(results.get_Categories, results.get_Posts);

                return reply.view("post", {
                    user,
                    post,
                    categories: categories,
                    dates: dateGetter.getDates(results.get_Posts),
                    next_post: getNextPost(slug_name, results.get_Posts),
                    previous_post: getPreviousPost(slug_name, results.get_Posts)
                });
            });
        }
    }
];

function getNextPost(slug, posts) {
    var index = _.findIndex(posts, {"title_slug": slug});
    return posts[index + 1];
}

function getPreviousPost(slug, posts) {
    var index = _.findIndex(posts, {"title_slug": slug});
    return posts[index - 1];
}

module.exports = routes;