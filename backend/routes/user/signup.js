const User = mechanic.models["User"];

const Register = require("./controllers/signup");

module.exports = [
    {
        method: "GET",
        path: "/user/signup",
        handler: (request, reply) => {

            // Handle if user is already logged in
            const user = request.session.get("user");
            if (user) {
                return reply.redirect("/");
            }

            const errors = request.session.flash("error");
            if (errors && errors.length > 0) {
                return reply.view("user/sign_up", {
                    error: errors.join(",")
                });
            }

            return reply.view("user/sign_up");
        }
    },
    {
        method: "POST",
        path: "/user/signup/create",
        handler: Register.create
    }
];