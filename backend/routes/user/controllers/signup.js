const User = mechanic.models["User"];

module.exports = {
    create: (request, reply) => {

        const username = request.payload["username"];
        const email = request.payload["email"];
        const password = request.payload["password_new"];

        if (!username) {
            request.session.flash("error", "Username field is empty.");
            return reply.redirect(request.info.referrer);
        }

        if (!email) {
            request.session.flash("error", "Email field is empty.");
            return reply.redirect(request.info.referrer);
        }

        if (!password) {
            request.session.flash("error", "Password field is empty.");
            return reply.redirect(request.info.referrer);
        }


        User.create({
            username: username,
            email: email,
            password: password
        }, (error, user) => {

            if (error) {
                console.log(error);
                request.session.flash("error", "Username or email already exist");
                return reply.redirect(request.info.referrer);
            }

            request.session.flash("success", "Successfully registerd");
            return reply.redirect(request.info.referrer);

        });
    }
}