/**
 * Created by goktug on 25/12/15.
 */
const User = mechanic.models["User"];

const Login = require("./controllers/signin");

module.exports = [
    {
        method: "GET",
        path: "/user/signin",
        handler: (request, reply) => {

            // Handle if user is already logged in
            const user = request.session.get("user");
            if (user) {
                return reply.redirect("/");
            }

            const errors = request.session.flash("error");
            if (errors && errors.length > 0) {
                return reply.view("user/sign_in", {
                    error: errors.join(",")
                });
            }

            return reply.view("user/sign_in");
        }
    },
    {
        method: "POST",
        path: "/user/signin/create",
        handler: Login.create
    }
]

