module.exports = [
    {
        method: "GET",
        path: "/user/signout",
        handler: (request, reply) => {
            request.session.clear("user");
            return reply.redirect("/");
        }
    }
]