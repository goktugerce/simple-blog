const MomentTZ = require("moment-timezone");

module.exports = function (date, format) {
    if (!date || !format) {
        return "";
    }
    return MomentTZ(date).tz("Europe/Istanbul").format(format);
};