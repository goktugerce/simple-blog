const _ = require("lodash");

function countPosts(categories, posts) {

    var categoriesLength = categories.length;
    for (var i = 0; i < categoriesLength; i++) {
        categories[i].post_count = countNames(categories[i], posts);
    }
    return categories;
}

function countNames(category, posts) {
    return _.filter(posts, function (post) {
        return post.category.name_slug == category.name_slug;
    }).length;
}

module.exports = countPosts;