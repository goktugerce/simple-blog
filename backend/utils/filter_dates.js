"use strict";
const _ = require("lodash");
const MomentTZ = require("moment-timezone");

const getDates = function (posts) {
    let dates = _.map(posts, "date");
    dates = _.map(dates, getPrettyDates);
    return _.uniq(dates).reverse();
}

function getPrettyDates(date) {
    if (!date) {
        return "";
    }

    return MomentTZ(date).tz("Europe/Istanbul").format("MMMM YY");
}

module.exports = {
    getDates: getDates
}