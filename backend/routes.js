/**
 * Created by goktug on 21/12/15.
 */

const Is = require("is_js");
const Post = mechanic.models["Post"];
const Async = require("async");
const User = mechanic.models["User"];
const Category = mechanic.models["Category"];
const _ = require("lodash");

const user_routes = require("./routes/user_routes.js");
const admin_routes = require("./routes/admin_routes.js");
const post_routes = require("./routes/post_routes.js");
const category_routes = require("./routes/category_routes.js");
const archive_routes = require("./routes/archive_routes.js");
const profile_routes = require("./routes/profile_routes.js");

const dateGetter = require("./utils/filter_dates");
const countPosts = require("./utils/count_posts")

module.exports = [
    {
        method: "GET",
        path: "/",
        handler: (request, reply) => {
            const page = 0;

            postListing(request, reply, page);
        }
    },
    {
        method: "GET",
        path: "/page/{pageNr}/",
        handler: (request, reply) => {
            const page = request.params.pageNr - 1;

            postListing(request, reply, page);
        }
    },
    {
        method: "GET",
        path: "/public/{param*}",
        handler: {
            directory: {
                path: "public"
            }
        }
    }
].concat(user_routes).concat(admin_routes).concat(post_routes).concat(category_routes).concat(archive_routes).concat(profile_routes);

function postListing(request, reply, page) {
    const user = request.session.get("user");
    Async.auto({
        get_Posts: function (callback) {
            Post.find({}, callback).populate("owner category");
        },
        get_Users: function (callback) {
            User.find({}, callback);
        },
        get_Categories: function (callback) {
            Category.find({}, callback);
        },
        count_Posts: function (callback) {
            Post.count(callback);
        }
    }, function (err, results) {
        if (err) {
            console.error("error here" + err);
        }

        var categories = countPosts(results.get_Categories, results.get_Posts);

        var posts = _.sortByOrder(results.get_Posts, ["date"], ["desc"]).slice(page*3, (page+1)*3);

        return reply.view("index", {
            user,
            page,
            posts,
            totalPosts: results.count_Posts,
            dates: dateGetter.getDates(results.get_Posts),
            categories: categories
        });
    });
}