const Mongoose = require("mongoose");

module.exports = () => {

    Mongoose.connect("mongodb://simpleblog:blog2705@ds035385.mongolab.com:35385/simpleblog");

    mechanic.models = {};

    mechanic.models["User"] = require("../app/model/user");
    mechanic.models["Category"] = require("../app/model/category");
    mechanic.models["Post"] = require("../app/model/post")
};