const Async = require("async");

module.exports = (server, plugins, callback) => {

    Async.eachSeries(plugins, (plugin, eachCallback) => {

        server.register({register: plugin.pluginModule, options: plugin.pluginOptions || {}}, eachCallback);
    }, callback);
};