"use strict";

const Hapi = require("hapi");
const Inert = require("inert");
const Vision = require("vision");
const Handlebars = require("handlebars");
const Yar = require("yar");

const PluginLoader = require("./utils/plugin_loader");
const DatabaseLoader = require("./utils/database_loader");

const server = new Hapi.Server();
server.connection({port: 3000});

/**
 * global: mechanic
 */

global.mechanic = {};

DatabaseLoader();
PluginLoader(server, [
    {pluginModule: Inert},
    {pluginModule: Vision},
    {
        pluginModule: Yar, pluginOptions:{cookieOptions: {password: "uywteryqwter", isSecure:false}}
    }
], (error) => {

    if (error) {
        throw error;
    }

    server.views({
        engines: {
            html: Handlebars
        },
        relativeTo: __dirname,
        layoutPath: "views/layout",
        layout: "default",
        path: "views",
        helpersPath: "views/helpers",
        partialsPath: "views/partials"
    });

    // Register the routes
    const routes = require("./backend/routes.js");

    // Tell the server about the defined routes
    server.route(routes);

    // Start the server
    server.start(function () {
        // Log to the console the host and port info
        console.log("Server running at: " + server.info.uri);
    });

});



