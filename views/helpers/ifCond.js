module.exports = function (v1, operator, v2, opts) {
    var isTrue = false;
    switch (operator) {
        case "==":
            isTrue = v1 == v2;
            break;
        case "!=":
            isTrue = v1 != v2;
            break;
        case "<":
            isTrue = v1 < v2;
            break;
        case "<=":
            isTrue = v1 <= v2;
            break;
        case ">":
            isTrue = v1 > v2;
            break;
        case ">=":
            isTrue = v1 >= v2;
            break;
        case "||":
            isTrue = v1 || v2;
            break;
        case "&&":
            isTrue = v1 && v2;
            break;
        case "%":
            isTrue = (v1 % v2) == 0;
            break;
        case "includes":
            isTrue = v1 ? v1[v2] : false;
            break;
        case "empty?":
            isTrue = v1 ? (v1[v2] ? v1[v2].length == 0 : true) : true;
            break;
        case "typeof?":
            isTrue = typeof v1 == v2;
            break;
        default:
            isTrue = false;
    }
    return isTrue ? opts.fn(this) : opts.inverse(this);
};