module.exports = function (page, target, total) {
    if (page == 0 && target == 0) {
        return "hidden";
    }
    if (target == 1 && (page+1) * 3 >= total) {
        return "hidden"
    }
    return "";
}