const Slug = require("slug");

Slug.defaults.mode = "rfc3986";

module.exports = function (title) {
    if (!title) {
        return "";
    }
    return Slug(title);
}