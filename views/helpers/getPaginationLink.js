module.exports = function (page, target) {
    if (page == 1 && target == 0) {
        return "/";
    }
    if (target == 0) {
        return "/page/" + (page) + "/";
    }
    return "/page/" + (page + 2) + "/";
}