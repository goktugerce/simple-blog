const limit = 128;

module.exports = function (content) {
    if (content.length < limit) {
        return content;
    }

    return content.substring(0, content.indexOf(".", limit)+1) + " Read more...";
}