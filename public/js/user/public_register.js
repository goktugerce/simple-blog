$(function () {

    var $errorDesc = $(".form-error");
    var $registerSubmit = $("#sign-up-form");

    $registerSubmit.submit(function () {
        var errors = [];

        var username = $("input[name='username']").val();
        var email = $("input[name='email']").val();
        var password1 = $("input[name='password_new']").val();
        var password2 = $("input[name='password_new_2']").val();

        if (!username) {
            // error check
            errors.push("Please enter your username.");
        }
        else if (username.length < 4){
            errors.push("Username should contain at least 4 characters.");
        }

        if (!email) {
            errors.push("Please enter a valid email address.");
        }

        if (password1 !== password2) {
            errors.push("Passwords should be identical");
        }
        else if (password1.length < 4) {
            errors.push("Password should contain at least 4 characters.");
        }

        if (errors.length > 0) {
            var errorList = errors.join("\n");
            $errorDesc.text(errorList).show();
            return false;
        }

        return true;
    });

});